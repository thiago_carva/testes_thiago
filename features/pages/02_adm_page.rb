class Admin < SitePrism::Page
    element :accounts_button, 'a[href="#ACCOUNTS"]'
    element :suppliers_button, 'a[href="https://www.phptravels.net/admin/accounts/suppliers/"]'
    element :add_button, 'button[type="submit"]'

    element :first_name, 'input[name="fname"]'
    element :last_name, 'input[name="lname"]'
    element :email, 'input[name="email"]'
    element :password, 'input[name="password"]'
    element :mobile, 'input[name="mobile"]'
    element :country, 'option[value="BR"]'
    element :address1, 'input[name="address1"]'
    element :adress2, 'input[name="address2"]'

    element :submit_button, 'button[class="btn btn-primary"]'


    def nav_menus
        accounts_button.click 
        suppliers_button.click
        sleep 3
        add_button.click
        sleep 3
    end

    def novo_cadastro
        first_name.set    Faker::Name.first_name
        last_name.set     Faker::Name.last_name
        email.set         FFaker::Internet.email
        password.set '123456'
        mobile.set        '98812-1515'
        country.select_option
        address1.set        FFaker::AddressBR.street
        adress2.set        Faker::Number.number(3)
        submit_button.click
        sleep 4
    end
end