class Home < SitePrism::Page   
    set_url '/admin'

    element :email, 'input[type="text"]'
    element :password, 'input[name="password"]'
    element :login_button, 'button[type="submit"]'
    
    def login(usuario, senha)
        email.set(usuario)
        password.set(senha)
        login_button.click
    end
end