Dado("que eu envie um post com os dados:") do |dados|
  @request = dados.rows_hash
end
  
Quando("envio a requisão do post") do
  @resultado = HTTParty.post(
  'http://fakerestapi.azurewebsites.net/swagger/ui/index#!/Books/Books_Post',
    body: @request.to_json
    )
end
  
Então("o código de retorno será {string}") do |return_code|
    expect(@resultado.response.code).to eql return_code
end