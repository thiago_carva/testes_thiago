Dado("que eu estou na pagina inicial do sistema") do
  @home = Home.new
  @home.load
  sleep 5
end
  
Quando("realizar o login com {string} e {string}") do |usuario, senha|
  @home.login(usuario, senha)
  sleep 5
end
  
Quando("crio um novo supplier") do
  @adm = Admin.new
  @adm.nav_menus
  @adm.novo_cadastro
end
  
Então("verifico se foi cadastrado com sucesso") do
  @texto = find('h4[class="ui-pnotify-title"]')
  expect(@texto.text).to eql 'CHANGES SAVED!'
  expect(page).to have_content (@email)
end