#language: pt


    Funcionalidade: Cadastramento Supplier
        Como administrador do sistema, desejo realizar o 
        cadastramento de novos Supliers

@novo_supplier
    Cenário: Cadastro com sucesso.
        Dado que eu estou na pagina inicial do sistema
        Quando realizar o login com "admin@phptravels.com" e "demoadmin"
            E crio um novo supplier
        Então verifico se foi cadastrado com sucesso