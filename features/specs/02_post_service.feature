#language: pt

Funcionalidade: Validar Status e Response

@post
    Cenário: Enviar Requisição POST
        Dado que eu envie um post com os dados:
            | ID        | 0                         |
            | Title     | Enviar requisição de POST |
            | DueDate   | 19/10/2018                |
            | Completed | true                      |
        Quando envio a requisão do post
        Então o código de retorno será "200"