Desafio QA Inmetrics

Introdução
Automatizar os testes funcionais que são planejados durante a Sprint é umas das muitas atribuições do Agile Tester.

Hoje, é comum as aplicações serem desenvolvidas para diversas plataformas (Android, iOS, Website) e consumirem uma webservice para adicionar uma camada de segurança, redução de código, entre outros benefícios.

Deste modo, além de realizar testes de front em websites ou no app mobile, é importante adicionar testes de serviço para obter maior qualidade no produto desenvolvido.

Desafios - WebSite

1 - Fazer o cadastro de um novo ACCOUNTS -> SUPPLIER no site https://www.phptravels.net/admin e validar que o mesmo foi adicionado a lista de SUPPLIERS cadastrados. Dados para login:

Desafios - WebService

1 - Escolher uma das APIs disponíveis em http://fakerestapi.azurewebsites.net/swagger/ui/index para enviar uma requisição do tipo POST. Validar o status code e o response do serviço

2 - Enviar um GET para https://jsonplaceholder.typicode.com/todos, validar o status code e exibir somente o id / title dos itens que estão como "completed: true"

Instalação de Bibliotecas do projeto
- Executar o bunlde install

Execução do Projeto Completo
 - cucumber

Execução de Cenários Web
- cucumber -t @novo_supplier

Execução de Cenários REST
- cucumber -t @post

